from django.db import models

class Category(models.Model):
    name = models.CharField(max_length = 100)
    def __str__(self):
        return self.name

# Create your models here.
class Book(models.Model):
    id = models.CharField(max_length = 20, primary_key = True)
    title = models.CharField(max_length = 200)
    subtitle = models.CharField(max_length = 350)
    authors = models.CharField(max_length = 200)
    publisher = models.CharField(max_length = 200)
    published_date = models.DateField("Date Published")
    category = models.ManyToManyField(Category, max_length = 100)
    distribution_expense = models.FloatField()