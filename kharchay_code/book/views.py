from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def book(request):
    #return HttpResponse("This is a testing phase")
    return render(request, 'books/index.html')

def display(request):
    return render(request, 'books/display_books.html')